TIPfun
======
(**T**umblr and **I**nstagram **fun** stuff with **P**interest-like endless scroll interface)

How to use:
----------
* Clone it
* Set up virtual env and install the requirements:
```
pip install -r requirements.txt
```
* Set up environment variables, or, if you use foreman like I do, just create .env file with these variables:
```
DATABASE_URL=<your database URL, e.g. postgres://user:password@localhost/database_name>
TUMBLR_CONSUMER_KEY=<your TUMBLR_CONSUMER_KEY>
TUMBLR_CONSUMER_SECRET=<your TUMBLR_CONSUMER_SECRET>
TUMBLR_OAUTH_TOKEN=<your TUMBLR_OAUTH_TOKEN>
TUMBLR_OAUTH_SECRET=<your TUMBLR_OAUTH_SECRET>
INSTAGRAM_CLIENT_ID=<your INSTAGRAM_CLIENT_ID>
INSTAGRAM_CLIENT_SECRET=<your INSTAGRAM_CLIENT_SECRET>
```
* run syncdb and migrations
```
./manage.py syncdb
./manage.py migrate
```
* Fetch the data from Tumblr and Instagram:
```
./manage.py grab_data
```
* Run the server with
```
./manage.py runserver
```
or with foreman
```
foreman start web
```
* Open your browser at http://localhost:8000/pins/
* That's all. Now you can see the funny stuff from Tumblr and Instagram

Tests:
-----
IMO, nothing to test at this point.