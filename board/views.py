from endless_pagination.views import AjaxListView

from .models import Pin
from .forms import SearchForm


class PinListView(AjaxListView):
    template_name = 'board/pins.html'
    page_template = 'board/pins_page.html'
    model = Pin

    def get_queryset(self):
        qs = super(PinListView, self).get_queryset()
        query = self.request.GET.get('query')
        if query:
            qs = qs.filter(text__icontains=query)
        return qs

    def get_context_data(self, **kwargs):
        context = super(PinListView, self).get_context_data(**kwargs)
        form = SearchForm(self.request.GET or None)
        context.update({'form': form})
        return context
