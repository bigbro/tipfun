from datetime import datetime
import logging

from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils.html import strip_tags
from instagram.client import InstagramAPI
from pytumblr import TumblrRestClient

from board.models import Pin


log = logging.getLogger('commands.grab_data')


class Command(BaseCommand):

    def handle(self, *args, **kwargs):
        log.info('Grabbing some Tumblt data')
        self._grab_tumblr_data()
        log.info('Grabbing some Instagram data')
        self._grab_instagram_data()

    def _grab_tumblr_data(self):

        client = TumblrRestClient(settings.TUMBLR_CONSUMER_KEY,
                                  settings.TUMBLR_CONSUMER_SECRET,
                                  settings.TUMBLR_OAUTH_TOKEN,
                                  settings.TUMBLR_OAUTH_SECRET)

        tags = ['funny-pictures', 'Comics', 'LOL', 'webcomics']

        for tag in tags:
            data = client.tagged(tag)
            for post in data:
                if 'photos' not in post:
                    continue
                pin, created = Pin.objects.get_or_create(url=post['short_url'])
                if created:
                    pin.img_url = post['photos'][0]['original_size']['url']
                    pin.text = strip_tags(post['caption'])[:255]
                    pin.published_at = datetime.fromtimestamp(post['timestamp'])
                    pin.save()

    def _grab_instagram_data(self):

        api = InstagramAPI(client_id=settings.INSTAGRAM_CLIENT_ID,
                           client_secret=settings.INSTAGRAM_CLIENT_SECRET)

        tags = ['funnypics', 'lol', 'rofl', 'lmfao']

        for tag in tags:
            data, _ = api.tag_recent_media(tag_name=tag, count=100)
            for post in data:
                pin, created = Pin.objects.get_or_create(url=post.link)
                if created:
                    pin.img_url = post.images['standard_resolution'].url
                    if hasattr(post.caption, 'text'):
                        pin.text = strip_tags(post.caption.text)[:255]
                    pin.published_at = post.created_time
                    pin.save()
