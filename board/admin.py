from django.contrib import admin

from .models import Pin


class PinAdmin(admin.ModelAdmin):
    list_display = ['url', 'img_url', 'text', 'published_at']

admin.site.register(Pin, PinAdmin)
