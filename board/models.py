from django.db import models


class Pin(models.Model):

    url = models.URLField(null=True, db_index=True)
    img_url = models.URLField(null=True, db_index=True)
    text = models.CharField(max_length=255, blank=True, null=True)
    published_at = models.DateTimeField(null=True)

    def __unicode__(self):
        return self.url

    class Meta:
        ordering = ['-published_at']