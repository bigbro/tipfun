# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Pin'
        db.create_table(u'board_pin', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('source', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, db_index=True)),
            ('img_url', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, db_index=True)),
            ('text', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('published_at', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'board', ['Pin'])


    def backwards(self, orm):
        # Deleting model 'Pin'
        db.delete_table(u'board_pin')


    models = {
        u'board.pin': {
            'Meta': {'object_name': 'Pin'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'db_index': 'True'}),
            'published_at': ('django.db.models.fields.DateTimeField', [], {}),
            'source': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'db_index': 'True'})
        }
    }

    complete_apps = ['board']