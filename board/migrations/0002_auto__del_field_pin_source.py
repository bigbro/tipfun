# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Pin.source'
        db.delete_column(u'board_pin', 'source')


    def backwards(self, orm):
        # Adding field 'Pin.source'
        db.add_column(u'board_pin', 'source',
                      self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=1),
                      keep_default=False)


    models = {
        u'board.pin': {
            'Meta': {'object_name': 'Pin'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'db_index': 'True'}),
            'published_at': ('django.db.models.fields.DateTimeField', [], {}),
            'text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'db_index': 'True'})
        }
    }

    complete_apps = ['board']