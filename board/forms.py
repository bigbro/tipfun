
from django import forms


class SearchForm(forms.Form):
    """
    Pin cards search form
    """

    query = forms.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': 'Search for ...',
                   'form': 'search_form'}),
        required=False)
